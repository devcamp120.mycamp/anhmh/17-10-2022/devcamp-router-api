//Câu lệnh này tương tự import 'express'; Dùng để import thư viện vào project
const { response } = require("express");
const express = require("express");

//Khởi tạo express
const app = express();

//Khai báo cổng của project
const port = 8000;

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào! Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
    })
})

app.get("/get-method", (request, response)  => {
    response.status(200).json({
        info: "GET method"
    })
})

app.post("/post-method", (request, response) => {
    response.status(200).json({
        info: "POST method"
    })
})

app.put("/put-method", (request, response) => {
    response.status(200).json({
        info: "PUT method"
    })
})

app.delete("/delete-method", (request, response) => {
    response.status(200).json({
        info: "DELETE method"
    })
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})